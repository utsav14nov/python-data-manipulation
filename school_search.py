import csv
import time
import sys

# Read Csv
# return reader
def getCsvReader():
	csvFile = open("school_data.csv", "r")
	csvReader = csv.DictReader(csvFile)
	return csvReader

# Add fullname to the lict of schools
# return null
def addFullSchoolNameInEachRecord(listOfSchools):
	for i in range(len(listOfSchools)):
		listOfSchools[i]['fullName'] = str(listOfSchools[i]['SCHNAM05']) + " " + str(listOfSchools[i]['LCITY05']) +  " " + str(listOfSchools[i]['LSTATE05'])



# Shift the lift to right and insert
# return null
def shiftListFromI(matchList,matchListScore,i,score,name):
	endPoint = len(matchListScore)-2
	while(endPoint >=i):
		matchListScore[endPoint+1] = matchListScore[endPoint]
		matchList[endPoint+1] = matchList[endPoint]
		endPoint=endPoint-1

	matchListScore[i] = score
	matchList[i] = name

# Sets score at the correct position in list
# return null
def orderMatchList(name,score,matchList,matchListScore):
	i=0
	while(i<3):
		if(score>matchListScore[i]):
			shiftListFromI(matchList,matchListScore,i,score,name)
			break
		i=i+1

# Calculate score for the query for a individual record
# return score
def getScore(keywords,name):
	score = 0
	for keyword in keywords:
		if(keyword.lower() in name.lower()):
			score = score + 1
	return score


# Search for the given query in file
# return list of first three closest match
def search(listOfSchools,query):
	keywords = query.split(" ")
	exactCount = 0

	matchList = ['']*3
	matchListScore = [0]*3

	for row in listOfSchools:
		score = getScore(keywords,row['fullName'])
		if(score > matchListScore[2]):
			orderMatchList(row['fullName'],score,matchList,matchListScore)

	return matchList


# Load data from CSV annd Print search list for the given query
# return null
def search_schools(query): 
	csvReader = getCsvReader()
	listOfSchools = list(csvReader)
	
	addFullSchoolNameInEachRecord(listOfSchools)
	
	start_time = time.time()
	
	result = search(listOfSchools,query)
	
	end_time = time.time()
			 
	timeDiff = (end_time-start_time)*1000

	print("Results for "+"\""+str(query)+"\"" + "(search took: "+str(timeDiff)+"ms)")
	count = 1
	for name in result:
		print(str(count)+". "+str(name))
		count=count+1

#Code execution starts here		
if __name__ == "__main__":
	n = len(sys.argv)
	if(n > 1):
		search_schools(sys.argv[1])
	else:
		print("Please provide search term ( python school_search.py \"jefferson belleville\") ")
