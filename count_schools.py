import csv
import itertools

# Read Csv
# return reader
def getCsvReader():
	csvFile = open("school_data.csv", "r")
	csvReader = csv.DictReader(csvFile)
	return csvReader

# Print Group result
# return null
def printGroupCount(groupData):
	for key,group in groupData:
		print(key+": "+str(len(list(group))))

# Group data by given csv column
# return group
def groupByKey(listOfSchools,key):
	listOfSchoolSorted = sorted(listOfSchools, key=lambda x: x[key]) 
	return itertools.groupby(listOfSchoolSorted, key=lambda x: x[key])

# Calculate group with max length
def getMaxGroupLength(groupData):
	largetGroup = ''
	maxGroupLength = 0
	uniqueCount = 0

	for key,group in groupData:
		uniqueCount = uniqueCount+1
		groupLength = len(list(group))
		if(groupLength >= maxGroupLength):
			maxGroupLength = groupLength
			largetGroup = key

	return({"largetGroup":largetGroup,"length":maxGroupLength,"count":uniqueCount})

# Load data from CSV annd Print result
# return null
def print_counts():
	csvReader = getCsvReader()
	listOfSchools = list(csvReader)
	

	groupByStateData = groupByKey(listOfSchools,'LSTATE05')
	groupByMLocaleData = groupByKey(listOfSchools,'MLOCALE')
	groupByCityData = groupByKey(listOfSchools,'LCITY05')

	print("Total Schools: "+str(len(listOfSchools)))
	
	print("Schools by State:")
	printGroupCount(groupByStateData)
	
	print("Schools by Metro-centric locale:")
	printGroupCount(groupByMLocaleData)
	
	maxCityGroup =  getMaxGroupLength(groupByCityData)
	print("City with most schools: "+str(maxCityGroup['largetGroup'])+" ("+str(maxCityGroup['length'])+" schools)")

	print("Unique cities with at least one school: "+str(maxCityGroup['count']))

#Code execution starts here	
if __name__ == "__main__": 	
    print_counts()	